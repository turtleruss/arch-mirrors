import argparse
import pathlib

# https://towardsdatascience.com/dynamically-add-arguments-to-argparse-python-patterns-a439121abc39

parser = argparse.ArgumentParser("arch-mirrors")
subparsers = parser.add_subparsers(help="Sub-commands help")

parser.add_argument(
	"--verbose",
	default=False,
	type=bool,
	help="Turns on verbose output",
)

def load_arguments():
	return parser.parse_args()
